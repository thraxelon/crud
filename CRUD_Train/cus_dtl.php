<?php
include_once "config/db.php";
include_once "obj/customer.php";

$id = isset($_GET['cid']) ? $_GET['cid'] : die('ERROR: You can\'t reach this page this way. <br> 
												 Please do it the  <a href="cus_list.php">right way</a>.');
$edt = isset($_GET['edt']) ? $_GET['edt'] : "no";
$rdo= "";
if ($edt == "no") {
	$rdo = 'readonly = "readonly"';
}else{
	$rdo = '';
}
$database = new DB();
$db = $database->getConnection();

$customer = new Customer($db);
$stmt = $customer->summonOne($id);
$rowResult = $stmt->fetch(PDO::FETCH_ASSOC);
extract($rowResult);

?>

<html>
	<head>
		<h1>DETAIL CUSTOMER</h1><br>
		<a href="cus_list.php"><-Go back</a><br><br>
	</head>
	<body>
		<form action="cus_upd.php" method="POST">
			<table>
				<tr>
					<td>Account No</td>
					<td>:</td>
					<td><input type="text" name="accno" value="<?php echo $acc_no; ?>" readonly></input></td>
				</tr>
				<tr>
					<td>Company Name</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="cname" value="<?php echo $cname; ?>"></input></td>
				</tr>
				<tr>
					<td>Address</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="adr1" value="<?php echo $address1; ?>"></input></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="text" <?php echo $rdo; ?> name="adr2" value="<?php echo $address2; ?>"></input></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="text" <?php echo $rdo; ?> name="adr3" value="<?php echo $address3; ?>"></input></td>
				</tr>
				<tr>
					<td>Person in Charge</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="attn" value="<?php echo $attn; ?>"></input></td>
				</tr>
				<tr>
					<td>Contact</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="contact" value="<?php echo $contact; ?>"></input></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><?php
							if ($edt=="no") {
								
							}else{
								echo '<input  type="submit" style="visibility: visible;" value="Submit">';
							}
					?>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
