<?php
echo "<html><head>
		<h1>ADD NEW CUSTOMER</h1><br>
		<a href='cus_list.php'><--goback</a><br><br>
	</head>";
//start including database and customer classes
include_once "config/db.php";
include_once "obj/customer.php";

//try to connect to db
$database = new DB();
$db = $database->getConnection();

//transfer connection to subclasses
$customer = new Customer($db);

//If submitted, get [POST] data
if($_POST){
	$customer->accno = $_POST['accno'];
	$customer->cname = $_POST['cname'];
	$customer->adr1 = $_POST['adr1'];
	$customer->adr2 = $_POST['adr2'];
	$customer->adr3 = $_POST['adr3'];
	$customer->attn = $_POST['attn'];
	$customer->phone = $_POST['phone'];



	//create new customer data
	if($customer->create()){
		echo "<div>Customer has been added</div>
		<br>";
	}else{
		echo "<div>Customer has not been added</div>";
	}
}


?>


	<body>
		<div>
			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
				<table>
					<tr>
						<td>Account No.</td>
						<td>:  <input type="text" name="accno"> </td>
					</tr>
					<tr>
						<td>Company Name</td>
						<td>:  <input type="text" name="cname"></td>
					</tr>
					<tr>
						<td>Address 1</td>
						<td>:  <input type="text" name="adr1"></td>
					</tr>
					<tr>
						<td>Address 2</td>
						<td>:  <input type="text" name="adr2"></td>
					</tr>
					<tr>
						<td>Address 3</td>
						<td>:  <input type="text" name="adr3"></td>
					</tr>
					<tr>
						<td>Attention</td>
						<td>:  <input type="text" name="attn"></td>
					</tr>
					<tr>
						<td>Contact</td>
						<td>:  <input type="text" name="phone"></td>
					</tr>
					<tr>
						<td></td>
						<td align="right">  <button type="submit" name="submit">Create</button></td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>