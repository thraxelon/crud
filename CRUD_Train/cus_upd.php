<?php
include_once 'config/db.php';
include_once 'obj/customer.php';

$database = new DB();
$db = $database->getConnection();

$customer = new Customer($db);

#define customer variable for ease of access

$customer->accno = $_POST['accno'];
$customer->cname = $_POST['cname'];
$customer->adr1 = $_POST['adr1'];
$customer->adr2 = $_POST['adr2'];
$customer->adr3 = $_POST['adr3'];
$customer->attn = $_POST['attn'];
$customer->phone = $_POST['contact'];

if ($customer->update()) {
	Echo "Customer has been successfully updated.";
}else{
	Echo "Failed to update Customer data.";
}
Echo "<br> Click <a href='cus_list.php'>here</a> to return.";
?>