-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 27, 2020 at 03:44 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dio_crud`
--
CREATE DATABASE `dio_crud` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dio_crud`;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `cid` int(4) NOT NULL AUTO_INCREMENT,
  `acc_no` varchar(8) NOT NULL,
  `cname` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `address3` varchar(255) DEFAULT NULL,
  `attn` varchar(50) DEFAULT NULL,
  `contact` varchar(16) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cid`, `acc_no`, `cname`, `address1`, `address2`, `address3`, `attn`, `contact`, `date_added`) VALUES
(5, '310A/001', 'PT. Ahmad Basuusuu', 'Jalan Jakarta', 'Blok Sekian RT Sekian', 'Jakarta', 'Ahmad', '0812 1234 5678', '2020-01-23 02:04:15');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
